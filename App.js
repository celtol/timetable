import * as Expo from 'expo';
import { SQLite } from 'expo';
import React, { Component } from 'react';
import { 
    StyleSheet, 
    View, 
    TextInput,
    Picker,
    TouchableHighlight,
    Modal,
    Text,
    Alert,
    AppRegistry,
    Image,
    FlatList,
    TouchableOpacity} from 'react-native';
import { Button } from 'react-native-elements';
import DateTimePicker from 'react-native-modal-datetime-picker';
import DateTimePicker2 from 'react-native-modal-datetime-picker';
import ActionButton from 'react-native-action-button';

import { Foundation } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';

const db = SQLite.openDatabase('db.db');

let activities = []; 

export default class App extends React.Component {
        constructor(props) {
            super(props);

            this.handlePress = this.handlePress.bind(this);
        }
        state={
            day: "1",
            daydisp: null,
            name: null,
            croom: null,
            starttime: null,
            endtime: null,
            isDateTimePickerVisible: false,
            isStartDateTimePickerVisible: false,
            isEndDateTimePickerVisible: false,
            modalAdd: false,
            modalChange: false,
            dayofweek: null,
            curtime: null,
            subj: [], 
            temp_id: null,          
        }
        handlePress(id) {

        }

        setAdd(visible) {
            this.setState({modalAdd: visible});
        }
        setChange(visible2) {
            this.setState({modalChange: visible2});
        }
      
        clearStates = () =>{
                this.setState({day: "1", name: null, croom: null, starttime: null, endtime: null,});
                this.setAdd(!this.state.modalAdd);
        }
        clearStates2 = () =>{
                this.setState({day: "1", name: null, croom: null, starttime: null, endtime: null,});
                this.setChange(!this.state.modalChange);
        }
        clearDB = () =>{
            db.transaction(
                tx => {
                tx.executeSql('delete from subjects');
                tx.executeSql('select * from subjects', [], (_, { rows }) =>
                        console.log(JSON.stringify(rows))
                    );
            });
        }
        showDB = () =>{
            db.transaction(
                tx => {
                tx.executeSql('select * from subjects', [], (_, { rows }) =>
                        console.log(JSON.stringify(rows))
                    );
            });
        }
        handleSubmit = () =>{
          db.transaction(
                tx => {
                    tx.executeSql('insert into subjects (day, name, croom, starttime, endtime) values (?, ?, ?, ?, ?)', [this.state.day, this.state.name, this.state.croom, this.state.starttime, this.state.endtime]);
                    tx.executeSql('select * from subjects', [], (_, { rows }) =>
                        console.log(JSON.stringify(rows))
                    );
                },
                null,
                this.clearStates
            );
        }
        handleChange = () =>{
          db.transaction(
                tx => {
                    tx.executeSql('insert into subjects (day, name, croom, starttime, endtime) values (?, ?, ?, ?, ?)', [this.state.day, this.state.name, this.state.croom, this.state.starttime, this.state.endtime]);
                    tx.executeSql('select * from subjects', [], (_, { rows }) =>
                        console.log(JSON.stringify(rows))
                    );
                },
                null,
                this.clearStates2
            );
        }
    _showStartTimePicker = () => this.setState({ isStartDateTimePickerVisible: true });
    _showEndTimePicker = () => this.setState({ isEndDateTimePickerVisible: true });
  
    _hideStartDateTimePicker = () => this.setState({ isStartDateTimePickerVisible: false });
    _hideEndDateTimePicker = () => this.setState({ isEndDateTimePickerVisible: false });
    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });
  
    _handleStartTimePicked = (date) => {
        var minutes = date.getMinutes()
        if (minutes < 10) {
            minutes = "0" + date.getMinutes()
        }
        this.setState({starttime: date.getHours() +":"+ minutes })
        this._hideStartDateTimePicker();
    };

    _handleEndTimePicked = (date) => {
        var minutes = date.getMinutes()
        if (minutes < 10) {
          minutes = "0" + date.getMinutes()
        }
        this.setState({endtime: date.getHours() +":"+ minutes })
        this._hideEndDateTimePicker();
    };
    
    async showActivities(){
      if (this.state.subj > 0) {
        this.setState({subj:[]});
      }
      db.transaction(
        tx => {
          tx.executeSql('select * from subjects where day = ?', [this.state.daydisp], (_, { rows: { _array } }) => {
            var arraysubj = _array
              this.setState({subj: arraysubj})
              activities = arraysubj
                        
          });
        });
    }
    showArray(){
      console.log(activities);
      console.log(activities.length)
    }
    
    
    componentDidMount(){
      var dw = new Date();
      var dw2 = dw.getDay().toString();
      this.setState({dayofweek: dw2})
      this.setState({daydisp: dw2})
      var minutes = dw.getMinutes()
      if (minutes < 10) {
        minutes = "0" + dw.getMinutes()
      }
      this.setState({curtime: dw.getHours() +":" + minutes })
        db.transaction(tx => {
          tx.executeSql(
            'create table if not exists subjects (id integer primary key not null, day text, name text, croom text, starttime string, endtime string);'
          );
        });
      this.showActivities()
    }

    render() {
        return (
            <View style={styles.container} >



                    <Picker
                        selectedValue={this.state.daydisp}
                        style={styles.picker}
                        onValueChange={(itemValue) => {
                            this.showActivities()
                            this.setState({daydisp: itemValue});
                            }}>
                            <Picker.Item style={styles.pickeritem} label="Poniedziałek" value="1" />
                            <Picker.Item style={styles.pickeritem} label="Wtorek" value="2" />
                            <Picker.Item style={styles.pickeritem} label="Środa" value="3" />
                            <Picker.Item style={styles.pickeritem} label="Czwartek" value="4" />
                            <Picker.Item style={styles.pickeritem} label="Piątek" value="5" />
                            <Picker.Item style={styles.pickeritem} label="Sobota" value="6" />
                            <Picker.Item style={styles.pickeritem} label="Niedziela" value="0" />
                    </Picker>
                    {/*<Button onPress={this.showArray} title="pokaz obiekt"/>
                    <Button onPress={this.clearDB} title="Wyczyść DB"/>
                    <Button onPress={this.showDB} title="pokaz baze"/>*/}
                    <View>
                    {this.state.subj.map((i, k) =>(
                      <TouchableOpacity style={styles.items} key={k} subjid={i.id} onLongPress={() => {
                        this.setChange(true);
                        this.handleLongPress
                      }}> 
                      <Text style={styles.item}>
                        <Text>
                          <Text style={styles.itname}>
                            {i.name}, Czas rozp.: {i.starttime}{'\n'}
                          </Text>
                        </Text>
                        <Text>
                          <Text style={styles.itromm}>
                            {i.croom}, Czas zak.: {i.endtime}
                          </Text>
                        </Text>
                      </Text> 
                        
                      </TouchableOpacity>
                      ))}
                    </View>



                    <Modal
                        animationType="slide"
                        transparent={false}
                        visible={this.state.modalAdd}
                        onRequestClose={this.clearStates}
                    >
                    <View style={{marginTop: 22}}>
                    <Picker
                        selectedValue={this.state.day}
                        style={styles.picker}
                        onValueChange={(itemValue) => {
                            this.setState({day: itemValue})
                        }}>
                            <Picker.Item style={styles.pickeritem} label="Poniedziałek" value="1" />
                            <Picker.Item style={styles.pickeritem} label="Wtorek" value="2" />
                            <Picker.Item style={styles.pickeritem} label="Środa" value="3" />
                            <Picker.Item style={styles.pickeritem} label="Czwartek" value="4" />
                            <Picker.Item style={styles.pickeritem} label="Piątek" value="5" />
                            <Picker.Item style={styles.pickeritem} label="Sobota" value="6" />
                            <Picker.Item style={styles.pickeritem} label="Niedziela" value="0" />
                    </Picker>
                    <View>
                    <Text style={styles.pickername}>
                      Nazwa: 
                    </Text>
                    <TextInput 
                      onChangeText={(text) => this.setState({name: text})}
                      value={this.state.name}
                       placeholder={"Nazwa"}
                       style={styles.inputborders}
                    />
                    <Text style={styles.pickername}>
                      Sala:
                    </Text>
                    <TextInput 
                        onChangeText={(text) => this.setState({croom: text})}
                        value={this.state.croom}
                        keyboardType='numeric'
                        placeholder={"Nr Sali"}
                        style={styles.inputborders}
                    />
                    </View>
                    <View style={{paddingTop: 5}}>
                    <Text style={styles.pickername}>
                     Godziny
                    </Text>
                    <TextInput 
                      value={this.state.starttime}
                      placeholder={"Godzina Rozpoczęcia"}
                      style={styles.inputborders}
                    />
                    <Button 
                      onPress={this._showStartTimePicker} 
                      title="Godzina Rozpoczęcia"
                      buttonStyle={styles.modalbuttons}
                      />
                    <TextInput 
                      value={this.state.endtime}
                      placeholder={"Godzina Zakończenia"}
                      style={styles.inputborders}
                    />
                    <Button 
                    onPress={this._showEndTimePicker} 
                    title="Godzina Zakończenia"
                        buttonStyle={styles.modalbuttons}
                    />
                    </View>
                    <View>
                    <Button onPress={this.handleSubmit} title="Dodaj"
                    buttonStyle={styles.modalbuttons}/>
                    <Button onPress={this.clearStates} title="Zamknij"
                    buttonStyle={styles.modalbuttons}/>
                    </View>
                    <DateTimePicker
                        isVisible={this.state.isStartDateTimePickerVisible}
                        onConfirm={this._handleStartTimePicked}
                        onCancel={this._hideStartDateTimePicker}
                        mode={'time'}
                        is24Hour = {true}
                    />
                    <DateTimePicker2
                        isVisible={this.state.isEndDateTimePickerVisible}
                        onConfirm={this._handleEndTimePicked}
                        onCancel={this._hideEndDateTimePicker}
                        mode={'time'}
                        is24Hour = {true}
                    />
                </View>
                </Modal>

                    <Modal
                        animationType="slide"
                        transparent={false}
                        visible={this.state.modalChange}
                        onRequestClose={this.clearStates2}
                    >
                    <View style={{marginTop: 22}}>
                    <Picker
                        selectedValue={this.state.day}
                        style={styles.picker}
                        onValueChange={(itemValue) => {
                            this.setState({day: itemValue})
                        }}>
                            <Picker.Item style={styles.pickeritem} label="Poniedziałek" value="1" />
                            <Picker.Item style={styles.pickeritem} label="Wtorek" value="2" />
                            <Picker.Item style={styles.pickeritem} label="Środa" value="3" />
                            <Picker.Item style={styles.pickeritem} label="Czwartek" value="4" />
                            <Picker.Item style={styles.pickeritem} label="Piątek" value="5" />
                            <Picker.Item style={styles.pickeritem} label="Sobota" value="6" />
                            <Picker.Item style={styles.pickeritem} label="Niedziela" value="0" />
                    </Picker>
                    <View>
                    <Text style={styles.pickername}>
                      Nazwa: 
                    </Text>
                    <TextInput 
                      onChangeText={(text) => this.setState({name: text})}
                      value={this.state.name}
                       placeholder={"Nazwa"}
                       style={styles.inputborders}
                    />
                    <Text style={styles.pickername}>
                      Sala:
                    </Text>
                    <TextInput 
                        onChangeText={(text) => this.setState({croom: text})}
                        value={this.state.croom}
                        keyboardType='numeric'
                        placeholder={"Nr Sali"}
                        style={styles.inputborders}
                    />
                    </View>
                    <View style={{paddingTop: 5}}>
                    <Text style={styles.pickername}>
                     Godziny
                    </Text>
                    <TextInput 
                      value={this.state.starttime}
                      placeholder={"Godzina Rozpoczęcia"}
                      style={styles.inputborders}
                    />
                    <Button 
                      onPress={this._showStartTimePicker} 
                      title="Godzina Rozpoczęcia"
                      buttonStyle={styles.modalbuttons}
                      />
                    <TextInput 
                      value={this.state.endtime}
                      placeholder={"Godzina Zakończenia"}
                      style={styles.inputborders}
                    />
                    <Button onPress={this._showEndTimePicker} title="Godzina Zakończenia"
                    buttonStyle={styles.modalbuttons}/>
                    </View>
                    <View>
                    <Button onPress={this.handleChange} title="Dodaj"
                    buttonStyle={styles.modalbuttons}/>
                    <Button onPress={this.clearStates} title="Zamknij"
                    buttonStyle={styles.modalbuttons}/>
                    </View>
                    <DateTimePicker
                        isVisible={this.state.isStartDateTimePickerVisible}
                        onConfirm={this._handleStartTimePicked}
                        onCancel={this._hideStartDateTimePicker}
                        mode={'time'}
                        is24Hour = {true}
                    />
                    <DateTimePicker2
                        isVisible={this.state.isEndDateTimePickerVisible}
                        onConfirm={this._handleEndTimePicked}
                        onCancel={this._hideEndDateTimePicker}
                        mode={'time'}
                        is24Hour = {true}
                    />
                </View>
                </Modal>
                
              
                <ActionButton
                    buttonColor="rgba(231,76,60,1)"
                    onPress={() => {
                        this.setAdd(true);
                    }}  
              />
            </View>
        );
    }
  
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingTop: Expo.Constants.statusBarHeight,
    },
    actionButtonIcon: {
        fontSize: 20,
        height: 22,
        color: 'white',
    },
    picker: {
      marginLeft: 10,
      paddingLeft: 10,
      paddingRight: 10,
      marginBottom: 5,
      height: 50,
      width: 385, 
    },
    items: {
      borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#000',
        marginLeft: 15,
      marginRight: 15,
      marginBottom: 5,
    },
    item: {
      fontSize: 20,
    },
    pickername: {
      paddingLeft: 15,
      fontSize: 18,
    },
    pickeritem: {
      fontSize: 18,
    },
    inputborders: {
      paddingRight: 15,
      paddingLeft: 15,

    },
    modalbuttons: {
        backgroundColor: "rgba(92, 99,216, 1)",
        width: 350,
        height: 45,
        borderColor: "transparent",
        borderWidth: 0,
        borderRadius: 5,
        paddingBottom: 5,          
    }
});
